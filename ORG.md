# Roles:

- Akash Prasad: Member 1
- Tyler Gee: Member 2
- Aishah Vakil: Member 3
- Vidyaaranya Macha: Member 4

# Project Artifacts:

1. [Github repo](https://github.com/Tillerpiggo/EasyPrint)
2. [Google Doc](https://docs.google.com/document/d/1gqYx0EqewGr57WvQLbk5aaXVpWEEVoaseTRknOySCU0/edit?usp=sharing)

# Communication Channels:

1. iMessage Group Chat

2. Emails:
   - Akash Prasad: akash.akpr@gmail.com
   - Tyler Gee: tyler.gee333@gmail.com
   - Aishah Vakil: aivakil@uw.edu
   - Vidyaaranya Macha: vmacha@uw.edu
